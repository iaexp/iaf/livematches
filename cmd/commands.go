package main

import (
	"log"

	pb "gitlab.com/iaexp/iaf/livematches/proto"
)

func selectCommand(c *pb.Command, t *Table, srv *pb.LivematchService_LivematchServer) {
	log.Println("add CMD")
	if c.AddGoalBlue {
		t.Match.ScoreBlue++
	} else if c.AddGoalRed {
		t.Match.ScoreRed++
	}
	t.Data <- t.Match
}
