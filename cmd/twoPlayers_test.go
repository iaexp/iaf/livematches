package main

import (
	"log"
	"testing"
	"time"

	pb "gitlab.com/iaexp/iaf/livematches/proto"
)

func TestTwoPlayers(t *testing.T) {
	//wg := sync.WaitGroup{}
	//wg.Add(1)
	go doStuff()
	time.Sleep(time.Millisecond * 1000)
	stream := newClient()
	stream.Send(&pb.Command{User: newUser("1")})
	log.Println(stream.Recv())
	log.Println(stream.Recv())
	log.Println(stream.Recv())
}

func doStuff() {
	stream := newClient()

	match := &pb.Match{}
	user := newUser("1")

	stream.Send(&pb.Command{User: user})

	match, _ = stream.Recv()
	time.Sleep(time.Second * 3)
	log.Println(match)
	log.Println(user.Id)

	stream.Send(&pb.Command{AddGoalBlue: true})
	match, _ = stream.Recv()
	log.Println(match)
	log.Println(user.Id)

	time.Sleep(time.Second * 3)
	stream.Send(&pb.Command{AddGoalBlue: true})
	match, _ = stream.Recv()
	log.Println(match)
	log.Println(user.Id)
}
