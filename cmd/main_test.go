package main

import (
	"context"
	pb "gitlab.com/iaexp/iaf/livematches/proto"
	"log"
	"math/rand"
	"strconv"
	"testing"
	"time"

	"google.golang.org/grpc"
)

func init() {
	log.SetFlags(log.Ltime | log.Lshortfile)
	*DevMode = true
	go main()
	time.Sleep(time.Second * 1)

}

// newUser creates a new user struct
func newUser(tableID string) *pb.User {
	return &pb.User{
		Id:           strconv.Itoa(rand.Int()),
		CurrentTable: tableID,
	}
}

// newClient opens a new client connection to the server
func newClient() pb.LivematchService_LivematchClient {
	conn, err := grpc.Dial(*port, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("can not connect with server %v", err)
	}
	client := pb.NewLivematchServiceClient(conn)
	stream, err := client.Livematch(context.Background())
	if err != nil {
		log.Fatalf("open stream error %v", err)
	}
	return stream
}

func TestStream(t *testing.T) {
	stream := newClient()

	match := &pb.Match{}
	user := newUser("1")

	stream.Send(&pb.Command{User: user})
	match, _ = stream.Recv()
	log.Println(match)

	stream.Send(&pb.Command{AddGoalBlue: true})
	match, _ = stream.Recv()
	log.Println(match)

	time.Sleep(time.Second * 1)
	stream.Send(&pb.Command{AddGoalBlue: true})
	match, _ = stream.Recv()
	log.Println(match)
}
