# STEP 1 build executable binary
FROM golang as builder

# Enable go modules
ENV GO111MODULE=on

# Copy prject folder and download deps and instll server
COPY . /go/src/gitlab.com/ieaxp/iaf/lifematches
WORKDIR /go/src/gitlab.com/ieaxp/iaf/lifematches
RUN go mod download && \
 cd /go/src/gitlab.com/ieaxp/iaf/lifematches/cmd && \
 CGO_ENABLED=0 GOOS=linux go build -ldflags "-s -w" -a -installsuffix cgo -o livematches .

# STEP 2 build a small image from scratch
FROM scratch
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

# Copy our static executable
COPY --from=builder /go/src/gitlab.com/ieaxp/iaf/lifematches/cmd/livematches .
CMD ./livematches