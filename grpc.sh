#!/bin/bash

cd $HOME/go/src/gitlab.com/iaexp/iaf/livematches
protoc -I proto/ proto/livematch.proto --go_out=plugins=grpc:proto
